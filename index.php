<!DOCTYPE html>
<html lang="pl">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Rezygnacja z podwyżki</title>

        <link rel="stylesheet" href="css/reset.css">
        <link rel="stylesheet" href="//www.upc.pl/etc/designs/upc-pe/css/responsive.min.bb50f8c20ca328e0807fc58c1aba40d4.css">
        <link rel="stylesheet" href="css/styles<?= isset($_GET['b2b']) ? '-b2b' : '' ?>.css">
        <script type="text/javascript" src="https://www.google.com/recaptcha/api/js/recaptcha_ajax.js"></script>
    </head>
    <body>
        <?php
            $fullFileName = __DIR__.'/config2/.disabled';
            if (isset($_GET['DPpjybEa9uQTmFwfJYxD'])) {
                if (isset($_GET['enable'])) {
                    unlink($fullFileName);
                } elseif (isset($_GET['disable'])) {
                    $url = $_GET['redirect'] ?? 'https://www.upc.pl';

                    $r = file_put_contents($fullFileName, date('Y-m-d H:i:s') . ";" . $url);
                }
            }
        ?>
        <?php 
        if (file_exists($fullFileName)) : 
            $url = explode(';', file_get_contents($fullFileName))[1];
        ?>
            <script>
                //window.top.location.href = '<?php echo $url ?>';
            </script>
        <?php endif; ?>
        <?php require_once "templates/resignation_form.php" ?>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <?php if (!empty($_COOKIE['test-data-source'])) : ?>
        <script src="https://app-t.upc.pl/podwyzka-rezygnacja/js/app.js?t=2"></script>
        <?php else : ?>
        <script src="js/app.js?t=2"></script>
        <?php endif ?>
        <script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit"
        async defer>
        </script>
        <script type="text/javascript">
          var onloadCallback = function() {
          };
        </script>
    </body>
</html>