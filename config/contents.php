<?php

$contents = [ 
    'errors' => array(
                'bad_request' => '<div class="panel">Błędne wywołanie akcji</div>',
                'no_action' => '<div class="panel">Brak zdefiniowanej akcji</div>',
                'unauthorized' => '<div class="panel"><div class="error-content">Niepoprawne dane uwierzytelnienia</div><a id="prev" href="#" class="btn-reset" >Cofnij</a></div>',
                'captcha' => '* Potwierdź, że nie jesteś robotem',
                'not_allowed' => '<div class="panel"><div class="header"><p>Ta strona nie jest dostępna.</p></div><a id="reset" class="btn-reset" href="#">Ponów</a></div>',
                'db' => '<div class="panel"><div class="error-content">Przepraszamy!<br>Wystąpił błąd. Spróbuj za chwilę.</div><a id="reset" href="#" class="btn-reset">Ponów</a></div>',
                'expired' => '<div class="panel"><div class="info"><p>Szanowny Kliencie,</p><p>Niestety termin bezpłatnej rezygnacji z usług minął %s o godzinie 23:59. 
							  W przypadku pytań skontaktuj się z Biurem Obsługi Klienta: 813 813 813.</p></div></div>'
            ),
            
    //headers per parameter ?f=A1
    'form_header_params' => [
        // 'A1' => 'FORMULARZ REZYGNACJI ZE ZMIANY USŁUG OD 1 WRZEŚNIA 2020',
        'default' => 'FORMULARZ REZYGNACJI ZE ZMIANY USŁUG OD 1 LUTEGO 2022 R.',
        'disabled' => 'FORMULARZ REZYGNACJI ZE ZMIANY USŁUG OD 1 LUTEGO 2022 R.'
    ],
    
    'forms' => array(
        //REZYGNACJA ZE ZMIANY PRĘDKOŚCI ORAZ ZMIANY OPŁATY ABONAMENTOWEJ OD 1 CZERWCA 2021 ROKU - grupa 52 [A1 (PI+SU)]
        52 => array(
            'header' => 'FORMULARZ REZYGNACJI ZE ZMIANY USŁUG ORAZ ZMIANY ABONAMENTU OD 1 CZERWCA 2021 R.',
            'text' => '<div class="panel">'.PHP_EOL.
                        '<div class="header">'.PHP_EOL.
                         '<p>Szanowny Kliencie,</p>'.PHP_EOL.
                        '</div>'.PHP_EOL.
                        '<div class="info">'.PHP_EOL.
                         '<p>Szybszy internet, który hula, pędzi i śmiga bez zacinania, na wielu urządzeniach jednocześnie. Dobrze mieć teraz taki internet do: pracy zdalnej, nauki online i rozrywki w sieci. Wierzymy, że będziesz z niego zadowolony!</p>'.PHP_EOL.
                         '<p>Jeśli już teraz wiesz, że szybszy internet światłowodowy nie jest dla Ciebie i chcesz aby dotychczasowa opłata abonamentowa po 1 czerwca 2021 r. pozostała bez zmian, kliknij poniższy przycisk.</p>'.PHP_EOL.
                        '</div>'.PHP_EOL.
                        '<input type="submit" name="confirm" class="submit" value="Potwierdzam" id="confirm">'.PHP_EOL.
                    '</div>'.PHP_EOL,
            'success' => '<div class="panel">'.PHP_EOL.
                            '<div class="info">'.PHP_EOL.
                             '<p>Dziękujemy, zgłoszenie zostało przyjęte do realizacji.</p>'.PHP_EOL.
                             '<p>Prędkość Twojego internetu światłowodowego oraz abonament po 1 czerwca 2021 r. pozostaną bez zmian.</p></div>'.PHP_EOL.
                            '<a id="reset" href="#" class="btn-reset">Powrót</a>'.PHP_EOL.
                        '</div>'.PHP_EOL,
            'in_progress' => '<div class="panel">'.PHP_EOL.
                                '<div class="info">'.PHP_EOL.
                                 '<p>Zgłoszenie jest w trakcie realizacji.</p>
                                 <p>Prędkość Twojego internetu światłowodowego oraz abonament po 1 czerwca 2021 r. pozostaną bez zmian.</p>'.PHP_EOL.
                                '</div>'.PHP_EOL.
                                '<a id="reset" href="#" class="btn-reset">Powrót</a>'.PHP_EOL.
                            '</div>'.PHP_EOL,
        ),
        //REZYGNACJA ZE ZMIANY PRĘDKOŚCI I ZMIANY PAKIETU TV ORAZ ZMIANY OPŁATY ABONAMENTOWEJ OD 1 CZERWCA 2021 grupa 53 [A2 (PI+SU+MU)]
        53 => array(
            'header' => 'FORMULARZ REZYGNACJI ZE ZMIANY USŁUG ORAZ ZMIANY ABONAMENTU OD 1 CZERWCA 2021 R.',
            'text' => '<div class="panel">'.PHP_EOL.
                        '<div class="header">'.PHP_EOL.
                         '<p>Szanowny Kliencie,</p>'.PHP_EOL.
                        '</div>'.PHP_EOL.
                        '<div class="info">'.PHP_EOL.
                         '<p>Z nowym pakietem Max zyskasz 39 świeżutkich pozycji na pilocie. A do tego internet światłowodowy, który hula, pędzi, śmiga bez zacinania z jeszcze większą prędkością. Wierzymy, że będziesz z tego zadowolony!</p>'.PHP_EOL.
                         '<p>Jeśli już teraz wiesz, że szybszy internet światłowodowy i zwiększony pakiet TV nie są dla Ciebie i chcesz aby dotychczasowa opłata abonamentowa po 1 czerwca 2021 r. pozostała bez zmian, kliknij poniższy przycisk.</p>'.PHP_EOL.
                        '</div>'.PHP_EOL.
                        '<input type="submit" name="confirm" class="submit" value="Potwierdzam" id="confirm">'.PHP_EOL.
                    '</div>'.PHP_EOL,
            'success' => '<div class="panel">'.PHP_EOL.
                            '<div class="info">'.PHP_EOL.
                             '<p>Dziękujemy, zgłoszenie zostało przyjęte do realizacji.</p>'.PHP_EOL.
                             '<p>Twoje usługi oraz abonament po 1 czerwca 2021 r. pozostaną bez zmian.</p></div>'.PHP_EOL.
                            '<a id="reset" href="#" class="btn-reset">Powrót</a>'.PHP_EOL.
                        '</div>'.PHP_EOL,
            'in_progress' => '<div class="panel">'.PHP_EOL.
                                '<div class="info">'.PHP_EOL.
                                 '<p>Zgłoszenie jest w trakcie realizacji.</p>
                                 <p>Twoje usługi oraz abonament po 1 czerwca 2021 r. pozostaną bez zmian.</p>'.PHP_EOL.
                                '</div>'.PHP_EOL.
                                '<a id="reset" href="#" class="btn-reset">Powrót</a>'.PHP_EOL.
                            '</div>'.PHP_EOL,
        ),
        //REZYGNACJA ZE ZMIANY PAKIETU TV ORAZ ZMIANY OPŁATY ABONAMENTOWEJ OD 1 CZERWCA 2021 grupa 54 [A3 (PI+MU)]
        54 => array(
            'header' => 'FORMULARZ REZYGNACJI ZE ZMIANY USŁUG ORAZ ZMIANY ABONAMENTU OD 1 CZERWCA 2021 R.',
            'text' => '<div class="panel">'.PHP_EOL.
                        '<div class="header">'.PHP_EOL.
                         '<p>Szanowny Kliencie,</p>'.PHP_EOL.
                        '</div>'.PHP_EOL.
                        '<div class="info">'.PHP_EOL.
                         '<p>Z bogatszym pakietem Max zyskasz 6 nowych kanałów filmowych i 7 kanałów dla ciekawych świata. Kibicujesz? Polubisz 7 nowych kanałów sportowych. A Twoje dzieciaki? Dostaną nowe kanały z bajkami. Wierzymy, że będziesz z tego zadowolony!</p>'.PHP_EOL.
                         '<p>Jeśli już teraz wiesz, że większy wybór kanałów nie jest dla Ciebie i chcesz aby dotychczasowa opłata abonamentowa po 1 czerwca 2021 r. pozostała bez zmian, kliknij poniższy przycisk.</p>'.PHP_EOL.
                        '</div>'.PHP_EOL.
                        '<input type="submit" name="confirm" class="submit" value="Potwierdzam" id="confirm">'.PHP_EOL.
                    '</div>'.PHP_EOL,
            'success' => '<div class="panel">'.PHP_EOL.
                            '<div class="info">'.PHP_EOL.
                             '<p>Dziękujemy, zgłoszenie zostało przyjęte do realizacji.</p>'.PHP_EOL.
                             '<p>Twój pakiet telewizyjny oraz abonament po 1 czerwca 2021 r. pozostaną bez zmian.</p></div>'.PHP_EOL.
                            '<a id="reset" href="#" class="btn-reset">Powrót</a>'.PHP_EOL.
                        '</div>'.PHP_EOL,
            'in_progress' => '<div class="panel">'.PHP_EOL.
                                '<div class="info">'.PHP_EOL.
                                 '<p>Zgłoszenie jest w trakcie realizacji.</p>
                                 <p>Twój pakiet telewizyjny oraz abonament po 1 czerwca 2021 r. pozostaną bez zmian.</p>'.PHP_EOL.
                                '</div>'.PHP_EOL.
                                '<a id="reset" href="#" class="btn-reset">Powrót</a>'.PHP_EOL.
                            '</div>'.PHP_EOL,
        ),
        //REZYGNACJA ZE ZMIANY PRĘDKOŚCI I ZMIANY PAKIETU TV ORAZ ZMIANY OPŁATY ABONAMENTOWEJ OD 1 CZERWCA 2021 grupa 55 [A4 (PI+SU+MXP)]
        55 => array(
            'header' => 'FORMULARZ REZYGNACJI ZE ZMIANY USŁUG ORAZ ZMIANY ABONAMENTU OD 1 CZERWCA 2021 R.',
            'text' => '<div class="panel">'.PHP_EOL.
                        '<div class="header">'.PHP_EOL.
                         '<p>Szanowny Kliencie,</p>'.PHP_EOL.
                        '</div>'.PHP_EOL.
                        '<div class="info">'.PHP_EOL.
                         '<p>Z nowym pakietem Max Premium zyskasz 10 świeżutkich pozycji na pilocie. A do tego internet światłowodowy, który hula, pędzi, śmiga bez zacinania z jeszcze większą prędkością. Wierzymy, że będziesz z tego zadowolony!</p>'.PHP_EOL.
                         '<p>Jeśli już teraz wiesz, że szybszy internet światłowodowy i zwiększony pakiet TV nie są dla Ciebie i chcesz aby dotychczasowa opłata abonamentowa po 1 czerwca 2021 r. pozostała bez zmian, kliknij poniższy przycisk.</p>'.PHP_EOL.
                        '</div>'.PHP_EOL.
                        '<input type="submit" name="confirm" class="submit" value="Potwierdzam" id="confirm">'.PHP_EOL.
                    '</div>'.PHP_EOL,
            'success' => '<div class="panel">'.PHP_EOL.
                            '<div class="info">'.PHP_EOL.
                             '<p>Dziękujemy, zgłoszenie zostało przyjęte do realizacji.</p>'.PHP_EOL.
                             '<p>Twoje usługi oraz abonament po 1 czerwca 2021 r. pozostaną bez zmian.</p></div>'.PHP_EOL.
                            '<a id="reset" href="#" class="btn-reset">Powrót</a>'.PHP_EOL.
                        '</div>'.PHP_EOL,
            'in_progress' => '<div class="panel">'.PHP_EOL.
                                '<div class="info">'.PHP_EOL.
                                 '<p>Zgłoszenie jest w trakcie realizacji.</p>
                                 <p>Twoje usługi oraz abonament po 1 czerwca 2021 r. pozostaną bez zmian.</p>'.PHP_EOL.
                                '</div>'.PHP_EOL.
                                '<a id="reset" href="#" class="btn-reset">Powrót</a>'.PHP_EOL.
                            '</div>'.PHP_EOL,
        ),
        //REZYGNACJA ZE ZMIANY PAKIETU TV ORAZ ZMIANY OPŁATY ABONAMENTOWEJ OD 1 CZERWCA 2021 grupa 56 [A5 (PI+MXP)]
        56 => array(
            'header' => 'FORMULARZ REZYGNACJI ZE ZMIANY USŁUG ORAZ ZMIANY ABONAMENTU OD 1 CZERWCA 2021 ROKU',
            'text' => '<div class="panel">'.PHP_EOL.
                        '<div class="header">'.PHP_EOL.
                         '<p>Szanowny Kliencie,</p>'.PHP_EOL.
                        '</div>'.PHP_EOL.
                        '<div class="info">'.PHP_EOL.
                         '<p>Z bogatszym pakietem Max Premium zyskasz 10 nowych kanałów filmowych. Miłośnicy dużej dawki adrenaliny znajdą tutaj kino sensacyjne, thrillery i horrory, a kinomaniacy – kino niezależne i autorskie. Wierzymy, że będziesz z tego zadowolony!</p>'.PHP_EOL.
                         '<p>Jeśli już teraz wiesz, że większy wybór kanałów nie jest dla Ciebie i chcesz aby dotychczasowa opłata abonamentowa po 1 czerwca 2021 r. pozostała bez zmian, kliknij poniższy przycisk.</p>'.PHP_EOL.
                        '</div>'.PHP_EOL.
                        '<input type="submit" name="confirm" class="submit" value="Potwierdzam" id="confirm">'.PHP_EOL.
                    '</div>'.PHP_EOL,
            'success' => '<div class="panel">'.PHP_EOL.
                            '<div class="info">'.PHP_EOL.
                             '<p>Dziękujemy, zgłoszenie zostało przyjęte do realizacji.</p>'.PHP_EOL.
                             '<p>Twój pakiet telewizyjny oraz abonament po 1 czerwca 2021 r. pozostaną bez zmian.</p></div>'.PHP_EOL.
                            '<a id="reset" href="#" class="btn-reset">Powrót</a>'.PHP_EOL.
                        '</div>'.PHP_EOL,
            'in_progress' => '<div class="panel">'.PHP_EOL.
                                '<div class="info">'.PHP_EOL.
                                 '<p>Zgłoszenie jest w trakcie realizacji.</p>
                                 <p>Twój pakiet telewizyjny oraz abonament po 1 czerwca 2021 r. pozostaną bez zmian.</p>'.PHP_EOL.
                                '</div>'.PHP_EOL.
                                '<a id="reset" href="#" class="btn-reset">Powrót</a>'.PHP_EOL.
                            '</div>'.PHP_EOL,
        ),
        //REZYGNACJA ZE ZMIANY PRĘDKOŚCI ORAZ ZMIANY OPŁATY ABONAMENTOWEJ OD 1 SIERPNIA 2021 ROKU – grupa 57 (A1 PI+SU)
        57 => array(
            'header' => 'FORMULARZ REZYGNACJI ZE ZMIANY USŁUG ORAZ ZMIANY ABONAMENTU OD 1 SIERPNIA 2021 R.',
            'text' => '<div class="panel">'.PHP_EOL.
                        '<div class="header">'.PHP_EOL.
                         '<p>Szanowny Kliencie,</p>'.PHP_EOL.
                        '</div>'.PHP_EOL.
                        '<div class="info">'.PHP_EOL.
                         '<p>Szybszy internet, który hula kiedy pracujesz i śmiga, kiedy się relaksujesz. Bez zacinania i na wielu urządzeniach jednocześnie. Wierzymy, że będziesz z niego zadowolony!</p>'.PHP_EOL.
                         '<p>Jeśli już teraz wiesz, że szybszy internet światłowodowy nie jest dla Ciebie i chcesz aby dotychczasowa opłata abonamentowa po 1 sierpnia 2021 r. pozostała bez zmian, kliknij poniższy przycisk.</p>'.PHP_EOL.
                        '</div>'.PHP_EOL.
                        '<input type="submit" name="confirm" class="submit" value="Potwierdzam" id="confirm">'.PHP_EOL.
                    '</div>'.PHP_EOL,
            'success' => '<div class="panel">'.PHP_EOL.
                            '<div class="info">'.PHP_EOL.
                             '<p>Dziękujemy, zgłoszenie zostało przyjęte do realizacji.</p>'.PHP_EOL.
                             '<p>Prędkość Twojego internetu światłowodowego oraz abonament po 1 sierpnia 2021 r. pozostaną bez zmian.</p></div>'.PHP_EOL.
                            '<a id="reset" href="#" class="btn-reset">Powrót</a>'.PHP_EOL.
                        '</div>'.PHP_EOL,
            'in_progress' => '<div class="panel">'.PHP_EOL.
                                '<div class="info">'.PHP_EOL.
                                 '<p>Zgłoszenie jest w trakcie realizacji.</p>
                                  <p>Prędkość Twojego internetu światłowodowego oraz abonament po 1 sierpnia 2021 r. pozostaną bez zmian.</p>'.PHP_EOL.
                                '</div>'.PHP_EOL.
                                '<a id="reset" href="#" class="btn-reset">Powrót</a>'.PHP_EOL.
                            '</div>'.PHP_EOL,
        ),
        //REZYGNACJA ZE ZMIANY PRĘDKOŚCI I ZMIANY PAKIETU TV ORAZ ZMIANY OPŁATY ABONAMENTOWEJ OD 1 SIERPNIA 2021 grupa 58 (A2 PI+SU+MU)
        58 => array(
            'header' => 'FORMULARZ REZYGNACJI ZE ZMIANY USŁUG ORAZ ZMIANY ABONAMENTU OD 1 SIERPNIA 2021 R.',
            'text' => '<div class="panel">'.PHP_EOL.
                        '<div class="header">'.PHP_EOL.
                         '<p>Szanowny Kliencie,</p>'.PHP_EOL.
                        '</div>'.PHP_EOL.
                        '<div class="info">'.PHP_EOL.
                         '<p>Z nowym pakietem Max zyskasz 39 świeżutkich pozycji na pilocie. A do tego internet światłowodowy, który hula, pędzi, śmiga bez zacinania z jeszcze większą prędkością. Wierzymy, że będziesz z tego zadowolony!</p>'.PHP_EOL.
                         '<p>Jeśli już teraz wiesz, że szybszy internet światłowodowy i zwiększony pakiet TV nie są dla Ciebie i chcesz aby dotychczasowa opłata abonamentowa po 1 sierpnia 2021 r. pozostała bez zmian, kliknij poniższy przycisk.</p>'.PHP_EOL.
                        '</div>'.PHP_EOL.
                        '<input type="submit" name="confirm" class="submit" value="Potwierdzam" id="confirm">'.PHP_EOL.
                    '</div>'.PHP_EOL,
            'success' => '<div class="panel">'.PHP_EOL.
                            '<div class="info">'.PHP_EOL.
                             '<p>Dziękujemy, zgłoszenie zostało przyjęte do realizacji.</p>'.PHP_EOL.
                             '<p>Twoje usługi oraz abonament po 1 sierpnia 2021 r. pozostaną bez zmian.</p></div>'.PHP_EOL.
                            '<a id="reset" href="#" class="btn-reset">Powrót</a>'.PHP_EOL.
                        '</div>'.PHP_EOL,
            'in_progress' => '<div class="panel">'.PHP_EOL.
                                '<div class="info">'.PHP_EOL.
                                 '<p>Zgłoszenie jest w trakcie realizacji.</p>
                                 <p>Twoje usługi oraz abonament po 1 sierpnia 2021 r. pozostaną bez zmian.</p>'.PHP_EOL.
                                '</div>'.PHP_EOL.
                                '<a id="reset" href="#" class="btn-reset">Powrót</a>'.PHP_EOL.
                            '</div>'.PHP_EOL,
        ),
        //REZYGNACJA ZE ZMIANY PAKIETU TV ORAZ ZMIANY OPŁATY ABONAMENTOWEJ OD 1 SIERPNIA 2021 grupa 59 (A3 PI+MU)
        59 => array(
            'header' => 'FORMULARZ REZYGNACJI ZE ZMIANY USŁUG ORAZ ZMIANY ABONAMENTU OD 1 SIERPNIA 2021 R.',
            'text' => '<div class="panel">'.PHP_EOL.
                        '<div class="header">'.PHP_EOL.
                         '<p>Szanowny Kliencie,</p>'.PHP_EOL.
                        '</div>'.PHP_EOL.
                        '<div class="info">'.PHP_EOL.
                         '<p>Z bogatszym pakietem Max zyskasz 5 nowych kanałów filmowych i 7 kanałów dla ciekawych świata. Kibicujesz? Polubisz 7 nowych kanałów sportowych. A Twoje dzieciaki? Dostaną nowe kanały z bajkami. Wierzymy, że będziesz z tego zadowolony!</p>'.PHP_EOL.
                         '<p>Jeśli już teraz wiesz, że większy wybór kanałów nie jest dla Ciebie i chcesz aby dotychczasowa opłata abonamentowa po 1 sierpnia 2021 r. pozostała bez zmian, kliknij poniższy przycisk.</p>'.PHP_EOL.
                        '</div>'.PHP_EOL.
                        '<input type="submit" name="confirm" class="submit" value="Potwierdzam" id="confirm">'.PHP_EOL.
                    '</div>'.PHP_EOL,
            'success' => '<div class="panel">'.PHP_EOL.
                            '<div class="info">'.PHP_EOL.
                             '<p>Dziękujemy, zgłoszenie zostało przyjęte do realizacji.</p>'.PHP_EOL.
                             '<p>Twój pakiet telewizyjny oraz abonament po 1 sierpnia 2021 r. pozostaną bez zmian.</p></div>'.PHP_EOL.
                            '<a id="reset" href="#" class="btn-reset">Powrót</a>'.PHP_EOL.
                        '</div>'.PHP_EOL,
            'in_progress' => '<div class="panel">'.PHP_EOL.
                                '<div class="info">'.PHP_EOL.
                                 '<p>Zgłoszenie jest w trakcie realizacji.</p>
                                 <p>Twój pakiet telewizyjny oraz abonament po 1 sierpnia 2021 r. pozostaną bez zmian.</p>'.PHP_EOL.
                                '</div>'.PHP_EOL.
                                '<a id="reset" href="#" class="btn-reset">Powrót</a>'.PHP_EOL.
                            '</div>'.PHP_EOL,
        ),
        //REZYGNACJA ZE ZMIANY PRĘDKOŚCI I ZMIANY PAKIETU TV ORAZ ZMIANY OPŁATY ABONAMENTOWEJ OD 1 SIERPNIA 2021 grupa 60 (A4 PI+SU+MXP)
        60 => array(
            'header' => 'FORMULARZ REZYGNACJI ZE ZMIANY USŁUG ORAZ ZMIANY ABONAMENTU OD 1 SIERPNIA 2021 R.',
            'text' => '<div class="panel">'.PHP_EOL.
                        '<div class="header">'.PHP_EOL.
                         '<p>Szanowny Kliencie,</p>'.PHP_EOL.
                        '</div>'.PHP_EOL.
                        '<div class="info">'.PHP_EOL.
                         '<p>Z nowym pakietem Max Premium zyskasz 10 świeżutkich pozycji na pilocie. A do tego internet światłowodowy, który hula, pędzi, śmiga bez zacinania z jeszcze większą prędkością. Wierzymy, że będziesz z tego zadowolony!</p>'.PHP_EOL.
                         '<p>Jeśli już teraz wiesz, że szybszy internet światłowodowy i zwiększony pakiet TV nie są dla Ciebie i chcesz aby dotychczasowa opłata abonamentowa po 1 sierpnia 2021 r. pozostała bez zmian, kliknij poniższy przycisk.</p>'.PHP_EOL.
                        '</div>'.PHP_EOL.
                        '<input type="submit" name="confirm" class="submit" value="Potwierdzam" id="confirm">'.PHP_EOL.
                    '</div>'.PHP_EOL,
            'success' => '<div class="panel">'.PHP_EOL.
                            '<div class="info">'.PHP_EOL.
                             '<p>Dziękujemy, zgłoszenie zostało przyjęte do realizacji.</p>'.PHP_EOL.
                             '<p>Twoje usługi oraz abonament po 1 sierpnia 2021 r. pozostaną bez zmian.</p></div>'.PHP_EOL.
                            '<a id="reset" href="#" class="btn-reset">Powrót</a>'.PHP_EOL.
                        '</div>'.PHP_EOL,
            'in_progress' => '<div class="panel">'.PHP_EOL.
                                '<div class="info">'.PHP_EOL.
                                 '<p>Zgłoszenie jest w trakcie realizacji.</p>
                                 <p>Twoje usługi oraz abonament po 1 sierpnia 2021 r. pozostaną bez zmian.</p>'.PHP_EOL.
                                '</div>'.PHP_EOL.
                                '<a id="reset" href="#" class="btn-reset">Powrót</a>'.PHP_EOL.
                            '</div>'.PHP_EOL,
        ),
        //REZYGNACJA ZE ZMIANY PAKIETU TV ORAZ ZMIANY OPŁATY ABONAMENTOWEJ OD 1 SIERPNIA 2021 grupa 61 (A5 PI+MXP)
        61 => array(
            'header' => 'FORMULARZ REZYGNACJI ZE ZMIANY USŁUG ORAZ ZMIANY ABONAMENTU OD 1 SIERPNIA 2021 ROKU',
            'text' => '<div class="panel">'.PHP_EOL.
                        '<div class="header">'.PHP_EOL.
                         '<p>Szanowny Kliencie,</p>'.PHP_EOL.
                        '</div>'.PHP_EOL.
                        '<div class="info">'.PHP_EOL.
                         '<p>Z bogatszym pakietem Max Premium zyskasz 10 nowych kanałów filmowych. Miłośnicy dużej dawki adrenaliny znajdą tutaj kino sensacyjne, thrillery i horrory, a kinomaniacy – kino niezależne i autorskie. Wierzymy, że będziesz z tego zadowolony!</p>'.PHP_EOL.
                         '<p>Jeśli już teraz wiesz, że większy wybór kanałów nie jest dla Ciebie i chcesz aby dotychczasowa opłata abonamentowa po 1 sierpnia 2021 r. pozostała bez zmian, kliknij poniższy przycisk.</p>'.PHP_EOL.
                        '</div>'.PHP_EOL.
                        '<input type="submit" name="confirm" class="submit" value="Potwierdzam" id="confirm">'.PHP_EOL.
                    '</div>'.PHP_EOL,
            'success' => '<div class="panel">'.PHP_EOL.
                            '<div class="info">'.PHP_EOL.
                             '<p>Dziękujemy, zgłoszenie zostało przyjęte do realizacji.</p>'.PHP_EOL.
                             '<p>Twój pakiet telewizyjny oraz abonament po 1 sierpnia 2021 r. pozostaną bez zmian.</p></div>'.PHP_EOL.
                            '<a id="reset" href="#" class="btn-reset">Powrót</a>'.PHP_EOL.
                        '</div>'.PHP_EOL,
            'in_progress' => '<div class="panel">'.PHP_EOL.
                                '<div class="info">'.PHP_EOL.
                                 '<p>Zgłoszenie jest w trakcie realizacji.</p>
                                 <p>Twój pakiet telewizyjny oraz abonament po 1 sierpnia 2021 r. pozostaną bez zmian.</p>'.PHP_EOL.
                                '</div>'.PHP_EOL.
                                '<a id="reset" href="#" class="btn-reset">Powrót</a>'.PHP_EOL.
                            '</div>'.PHP_EOL,
        ),
        //Segment 62 ( A1, PI+SU)
        62 => array(
            'header' => 'FORMULARZ REZYGNACJI ZE ZMIANY USŁUGI ORAZ ZMIANY ABONAMENTU <br> OD 1 LISTOPADA 2021 R.',
            'text' => '<div class="panel">'.PHP_EOL.
                        '<div class="header">'.PHP_EOL.
                         '<p>Dzień dobry,</p>'.PHP_EOL.
                        '</div>'.PHP_EOL.
                        '<div class="info">'.PHP_EOL.
                         '<p>Szybszy internet, który hula kiedy pracujesz i śmiga, kiedy się relaksujesz. Bez zacinania i na wielu urządzeniach jednocześnie. Wierzymy, że będziesz z niego zadowolony!</p>'.PHP_EOL.
                         '<p>Jeśli już teraz wiesz, że szybszy internet światłowodowy nie jest dla Ciebie i chcesz aby dotychczasowa opłata abonamentowa <br> po 1 listopada 2021 r. pozostała bez zmian, kliknij poniższy przycisk.</p>'.PHP_EOL.
                        '</div>'.PHP_EOL.
                        '<input type="submit" name="confirm" class="submit" value="Potwierdzam" id="confirm">'.PHP_EOL.
                    '</div>'.PHP_EOL,
            'success' => '<div class="panel">'.PHP_EOL.
                            '<div class="info">'.PHP_EOL.
                             '<p>Dziękujemy, zgłoszenie zostało przyjęte do realizacji.</p>'.PHP_EOL.
                             '<p>Prędkość Twojego internetu światłowodowego oraz abonament <br> po 1 listopada 2021 r. pozostaną bez zmian.</p></div>'.PHP_EOL.
                            '<a id="reset" href="#" class="btn-reset">Powrót</a>'.PHP_EOL.
                        '</div>'.PHP_EOL,
            'in_progress' => '<div class="panel">'.PHP_EOL.
                                '<div class="info">'.PHP_EOL.
                                 '<p>Otrzymaliśmy już Twoje poprzednie zgłoszenie i jest ono w trakcie realizacji.</p>
                                 <p>Prędkość Twojego internetu światłowodowego oraz abonament <br> po 1 listopada 2021 r. pozostaną bez zmian.</p>'.PHP_EOL.
                                '</div>'.PHP_EOL.
                                '<a id="reset" href="#" class="btn-reset">Powrót</a>'.PHP_EOL.
                            '</div>'.PHP_EOL,
        ),
        //Segment  63 (A2, PI+SU+MU)
        63 => array(
            'header' => 'FORMULARZ REZYGNACJI ZE ZMIANY USŁUG ORAZ ZMIANY ABONAMENTU <br> OD 1 LISTOPADA 2021 R.',
            'text' => '<div class="panel">'.PHP_EOL.
                        '<div class="header">'.PHP_EOL.
                         '<p>Dzień dobry,</p>'.PHP_EOL.
                        '</div>'.PHP_EOL.
                        '<div class="info">'.PHP_EOL.
                         '<p>Z nowym pakietem Max zyskasz 39 świeżutkich pozycji na pilocie. A do tego internet światłowodowy, który hula, pędzi, śmiga bez zacinania z jeszcze większą prędkością. Wierzymy, że będziesz z tego zadowolony!</p>'.PHP_EOL.
                         '<p>Jeśli już teraz wiesz, że szybszy internet światłowodowy i zwiększony pakiet TV nie są dla Ciebie i chcesz aby dotychczasowa opłata abonamentowa po 1 listopada 2021 r. pozostała bez zmian, kliknij poniższy przycisk.</p>'.PHP_EOL.
                        '</div>'.PHP_EOL.
                        '<input type="submit" name="confirm" class="submit" value="Potwierdzam" id="confirm">'.PHP_EOL.
                    '</div>'.PHP_EOL,
            'success' => '<div class="panel">'.PHP_EOL.
                            '<div class="info">'.PHP_EOL.
                             '<p>Dziękujemy, zgłoszenie zostało przyjęte do realizacji.</p>'.PHP_EOL.
                             '<p>Twoje usługi oraz abonament <br> po 1 listopada 2021 r. pozostaną bez zmian.</p></div>'.PHP_EOL.
                            '<a id="reset" href="#" class="btn-reset">Powrót</a>'.PHP_EOL.
                        '</div>'.PHP_EOL,
            'in_progress' => '<div class="panel">'.PHP_EOL.
                                '<div class="info">'.PHP_EOL.
                                 '<p>Otrzymaliśmy już Twoje poprzednie zgłoszenie i jest ono w trakcie realizacji.</p>
                                 <p>Twoje usługi oraz abonament <br> po 1 listopada 2021 r. pozostaną bez zmian.</p>'.PHP_EOL.
                                '</div>'.PHP_EOL.
                                '<a id="reset" href="#" class="btn-reset">Powrót</a>'.PHP_EOL.
                            '</div>'.PHP_EOL,
        ),
        //Segment 64 (A3, PI+MU)
        64 => array(
            'header' => 'FORMULARZ REZYGNACJI ZE ZMIANY USŁUGI ORAZ ZMIANY ABONAMENTU <br> OD 1 LISTOPADA 2021 R.',
            'text' => '<div class="panel">'.PHP_EOL.
                        '<div class="header">'.PHP_EOL.
                         '<p>Dzień dobry,</p>'.PHP_EOL.
                        '</div>'.PHP_EOL.
                        '<div class="info">'.PHP_EOL.
                         '<p>Z bogatszym pakietem Max zyskasz 5 nowych kanałów filmowych i 7 kanałów dla ciekawych świata. Kibicujesz? Polubisz 7 nowych kanałów sportowych. A Twoje dzieciaki? Dostaną nowe kanały z bajkami. Wierzymy, że będziesz z tego zadowolony!</p>'.PHP_EOL.
                         '<p>Jeśli już teraz wiesz, że większy wybór kanałów nie jest dla Ciebie i chcesz aby dotychczasowa opłata abonamentowa <br> po 1 listopada 2021 r. pozostała bez zmian, kliknij poniższy przycisk.</p>'.PHP_EOL.
                        '</div>'.PHP_EOL.
                        '<input type="submit" name="confirm" class="submit" value="Potwierdzam" id="confirm">'.PHP_EOL.
                    '</div>'.PHP_EOL,
            'success' => '<div class="panel">'.PHP_EOL.
                            '<div class="info">'.PHP_EOL.
                             '<p>Dziękujemy, zgłoszenie zostało przyjęte do realizacji.</p>'.PHP_EOL.
                             '<p>Twój pakiet telewizyjny oraz abonament <br> po 1 listopada 2021 r. pozostaną bez zmian.</p></div>'.PHP_EOL.
                            '<a id="reset" href="#" class="btn-reset">Powrót</a>'.PHP_EOL.
                        '</div>'.PHP_EOL,
            'in_progress' => '<div class="panel">'.PHP_EOL.
                                '<div class="info">'.PHP_EOL.
                                 '<p>Otrzymaliśmy już Twoje poprzednie zgłoszenie i jest ono w trakcie realizacji.</p>
                                 <p>Twój pakiet telewizyjny oraz abonament <br> po 1 listopada 2021 r. pozostaną bez zmian.</p>'.PHP_EOL.
                                '</div>'.PHP_EOL.
                                '<a id="reset" href="#" class="btn-reset">Powrót</a>'.PHP_EOL.
                            '</div>'.PHP_EOL,
        ),
        //Segment 65 (A4, PI+SU+MPU)
        65 => array(
            'header' => 'FORMULARZ REZYGNACJI ZE ZMIANY USŁUG ORAZ ZMIANY ABONAMENTU <br> OD 1 LISTOPADA 2021 R.',
            'text' => '<div class="panel">'.PHP_EOL.
                        '<div class="header">'.PHP_EOL.
                         '<p>Dzień dobry,</p>'.PHP_EOL.
                        '</div>'.PHP_EOL.
                        '<div class="info">'.PHP_EOL.
                         '<p>Z nowym pakietem Max Premium zyskasz 9 świeżutkich pozycji na pilocie. A do tego internet światłowodowy, który hula, pędzi, śmiga bez zacinania z jeszcze większą prędkością. Wierzymy, że będziesz z tego zadowolony!</p>'.PHP_EOL.
                         '<p>Jeśli już teraz wiesz, że szybszy internet światłowodowy i zwiększony pakiet TV nie są dla Ciebie i chcesz aby dotychczasowa opłata abonamentowa po 1 listopada 2021 r. pozostała bez zmian, kliknij poniższy przycisk.</p>'.PHP_EOL.
                        '</div>'.PHP_EOL.
                        '<input type="submit" name="confirm" class="submit" value="Potwierdzam" id="confirm">'.PHP_EOL.
                    '</div>'.PHP_EOL,
            'success' => '<div class="panel">'.PHP_EOL.
                            '<div class="info">'.PHP_EOL.
                             '<p>Dziękujemy, zgłoszenie zostało przyjęte do realizacji.</p>'.PHP_EOL.
                             '<p>Twoje usługi oraz abonament <br> po 1 listopada 2021 r. pozostaną bez zmian.</p></div>'.PHP_EOL.
                            '<a id="reset" href="#" class="btn-reset">Powrót</a>'.PHP_EOL.
                        '</div>'.PHP_EOL,
            'in_progress' => '<div class="panel">'.PHP_EOL.
                                '<div class="info">'.PHP_EOL.
                                 '<p>Otrzymaliśmy już Twoje poprzednie zgłoszenie i jest ono w trakcie realizacji.</p>
                                 <p>Twoje usługi oraz abonament <br> po 1 listopada 2021 r. pozostaną bez zmian.</p>'.PHP_EOL.
                                '</div>'.PHP_EOL.
                                '<a id="reset" href="#" class="btn-reset">Powrót</a>'.PHP_EOL.
                            '</div>'.PHP_EOL,
        ),
        //Segment 66 (A5, PI+MPU)
        66 => array(
            'header' => 'FORMULARZ REZYGNACJI ZE ZMIANY USŁUGI ORAZ ZMIANY ABONAMENTU <br> OD 1 LISTOPADA 2021 R.',
            'text' => '<div class="panel">'.PHP_EOL.
                        '<div class="header">'.PHP_EOL.
                         '<p>Dzień dobry,</p>'.PHP_EOL.
                        '</div>'.PHP_EOL.
                        '<div class="info">'.PHP_EOL.
                         '<p>Z bogatszym pakietem Max Premium zyskasz 9 nowych kanałów filmowych. Miłośnicy dużej dawki adrenaliny znajdą tutaj kino sensacyjne, thrillery i horrory, a kinomaniacy – kino niezależne i autorskie. Wierzymy, że będziesz z tego zadowolony!</p>'.PHP_EOL.
                         '<p>Jeśli już teraz wiesz, że większy wybór kanałów nie jest dla Ciebie i chcesz aby dotychczasowa opłata abonamentowa <br> po 1 listopada 2021 r. pozostała bez zmian, kliknij poniższy przycisk.</p>'.PHP_EOL.
                        '</div>'.PHP_EOL.
                        '<input type="submit" name="confirm" class="submit" value="Potwierdzam" id="confirm">'.PHP_EOL.
                    '</div>'.PHP_EOL,
            'success' => '<div class="panel">'.PHP_EOL.
                            '<div class="info">'.PHP_EOL.
                             '<p>Dziękujemy, zgłoszenie zostało przyjęte do realizacji.</p>'.PHP_EOL.
                             '<p>Twój pakiet telewizyjny oraz abonament <br> po 1 listopada 2021 r. pozostaną bez zmian.</p></div>'.PHP_EOL.
                            '<a id="reset" href="#" class="btn-reset">Powrót</a>'.PHP_EOL.
                        '</div>'.PHP_EOL,
            'in_progress' => '<div class="panel">'.PHP_EOL.
                                '<div class="info">'.PHP_EOL.
                                 '<p>Otrzymaliśmy już Twoje poprzednie zgłoszenie i jest ono w trakcie realizacji.</p>
                                 <p>Twój pakiet telewizyjny oraz abonament <br> po 1 listopada 2021 r. pozostaną bez zmian.</p>'.PHP_EOL.
                                '</div>'.PHP_EOL.
                                '<a id="reset" href="#" class="btn-reset">Powrót</a>'.PHP_EOL.
                            '</div>'.PHP_EOL,
        ),
        // Segment 67 (A1, PI+SU)
        67 => array(
            'header' => 'FORMULARZ REZYGNACJI ZE ZMIANY USŁUGI ORAZ ZMIANY ABONAMENTU<br> OD 1 LUTEGO 2022 R.',
            'text' => '<div class="panel">'.PHP_EOL.
                        '<div class="header">'.PHP_EOL.
                         '<p>Dzień dobry,</p>'.PHP_EOL.
                        '</div>'.PHP_EOL.
                        '<div class="info">'.PHP_EOL.
                         '<p>Szybszy internet, który hula kiedy pracujesz i śmiga, kiedy się relaksujesz. Bez zacinania i na wielu urządzeniach jednocześnie. Wierzymy, że będziesz z niego zadowolony!</p>'.PHP_EOL.
                         '<p>Jeśli już teraz wiesz, że szybszy internet światłowodowy nie jest dla Ciebie i chcesz aby dotychczasowa opłata abonamentowa <br>po 1 lutego 2022 r. pozostała bez zmian, kliknij poniższy przycisk.</p>'.PHP_EOL.
                        '</div>'.PHP_EOL.
                        '<input type="submit" name="confirm" class="submit" value="Potwierdzam" id="confirm">'.PHP_EOL.
                    '</div>'.PHP_EOL,
            'success' => '<div class="panel">'.PHP_EOL.
                            '<div class="info">'.PHP_EOL.
                             '<p>Dziękujemy, zgłoszenie zostało przyjęte do realizacji.</p>'.PHP_EOL.
                             '<p>Prędkość Twojego internetu światłowodowego oraz abonament <br>po 1 lutego 2022 r. pozostaną bez zmian.</p></div>'.PHP_EOL.
                            '<a id="reset" href="#" class="btn-reset">Powrót</a>'.PHP_EOL.
                        '</div>'.PHP_EOL,
            'in_progress' => '<div class="panel">'.PHP_EOL.
                                '<div class="info">'.PHP_EOL.
                                 '<p>Otrzymaliśmy już Twoje poprzednie zgłoszenie i jest ono w trakcie realizacji.</p>
                                 <p>Prędkość Twojego internetu światłowodowego oraz abonament <br>po 1 lutego 2022 r. pozostaną bez zmian.</p>'.PHP_EOL.
                                '</div>'.PHP_EOL.
                                '<a id="reset" href="#" class="btn-reset">Powrót</a>'.PHP_EOL.
                            '</div>'.PHP_EOL,
        ),
        // Segment 68 (A2, PI+SU+MU)
        68 => array(
            'header' => 'FORMULARZ REZYGNACJI ZE ZMIANY USŁUG ORAZ ZMIANY ABONAMENTU<br> OD 1 LUTEGO 2022 R.',
            'text' => '<div class="panel">'.PHP_EOL.
                        '<div class="header">'.PHP_EOL.
                         '<p>Dzień dobry,</p>'.PHP_EOL.
                        '</div>'.PHP_EOL.
                        '<div class="info">'.PHP_EOL.
                         '<p>Z nowym pakietem Max zyskasz 39 świeżutkich pozycji na pilocie. A do tego internet światłowodowy, który hula, pędzi, śmiga bez zacinania z jeszcze większą prędkością. Wierzymy, że będziesz z tego zadowolony!</p>'.PHP_EOL.
                         '<p>Jeśli już teraz wiesz, że szybszy internet światłowodowy i zwiększony pakiet TV nie są dla Ciebie i chcesz aby dotychczasowa opłata abonamentowa po 1 lutego 2022 r. pozostała bez zmian, kliknij poniższy przycisk.</p>'.PHP_EOL.
                        '</div>'.PHP_EOL.
                        '<input type="submit" name="confirm" class="submit" value="Potwierdzam" id="confirm">'.PHP_EOL.
                    '</div>'.PHP_EOL,
            'success' => '<div class="panel">'.PHP_EOL.
                            '<div class="info">'.PHP_EOL.
                             '<p>Dziękujemy, zgłoszenie zostało przyjęte do realizacji.</p>'.PHP_EOL.
                             '<p>Twoje usługi oraz abonament po 1 lutego 2022 r. pozostaną bez zmian.</p></div>'.PHP_EOL.
                            '<a id="reset" href="#" class="btn-reset">Powrót</a>'.PHP_EOL.
                        '</div>'.PHP_EOL,
            'in_progress' => '<div class="panel">'.PHP_EOL.
                                '<div class="info">'.PHP_EOL.
                                 '<p>Otrzymaliśmy już Twoje poprzednie zgłoszenie i jest ono w trakcie realizacji.</p>
                                 <p>Twoje usługi oraz abonament po 1 lutego 2022 r. pozostaną bez zmian.</p>'.PHP_EOL.
                                '</div>'.PHP_EOL.
                                '<a id="reset" href="#" class="btn-reset">Powrót</a>'.PHP_EOL.
                            '</div>'.PHP_EOL,
        ),
        // Segment 69 (A3, PI+MU)
        69 => array(
            'header' => 'FORMULARZ REZYGNACJI ZE ZMIANY USŁUGI ORAZ ZMIANY ABONAMENTU<br> OD 1 LUTEGO 2022 R.',
            'text' => '<div class="panel">'.PHP_EOL.
                        '<div class="header">'.PHP_EOL.
                         '<p>Dzień dobry,</p>'.PHP_EOL.
                        '</div>'.PHP_EOL.
                        '<div class="info">'.PHP_EOL.
                         '<p>Z bogatszym pakietem Max zyskasz 5 nowych kanałów filmowych i 7 kanałów dla ciekawych świata. Kibicujesz? Polubisz 7 nowych kanałów sportowych. A Twoje dzieciaki? Dostaną nowe kanały z bajkami. Wierzymy, że będziesz z tego zadowolony!</p>'.PHP_EOL.
                         '<p>Jeśli już teraz wiesz, że większy wybór kanałów nie jest dla Ciebie i chcesz aby dotychczasowa opłata abonamentowa po 1 lutego 2022 r. pozostała bez zmian, kliknij poniższy przycisk.</p>'.PHP_EOL.
                        '</div>'.PHP_EOL.
                        '<input type="submit" name="confirm" class="submit" value="Potwierdzam" id="confirm">'.PHP_EOL.
                    '</div>'.PHP_EOL,
            'success' => '<div class="panel">'.PHP_EOL.
                            '<div class="info">'.PHP_EOL.
                             '<p>Dziękujemy, zgłoszenie zostało przyjęte do realizacji.</p>'.PHP_EOL.
                             '<p>Twój pakiet telewizyjny oraz abonament po 1 lutego 2022 r. pozostaną bez zmian.</p></div>'.PHP_EOL.
                            '<a id="reset" href="#" class="btn-reset">Powrót</a>'.PHP_EOL.
                        '</div>'.PHP_EOL,
            'in_progress' => '<div class="panel">'.PHP_EOL.
                                '<div class="info">'.PHP_EOL.
                                 '<p>Otrzymaliśmy już Twoje poprzednie zgłoszenie i jest ono w trakcie realizacji.</p>
                                 <p>Twój pakiet telewizyjny oraz abonament po 1 lutego 2022 r. pozostaną bez zmian.</p>'.PHP_EOL.
                                '</div>'.PHP_EOL.
                                '<a id="reset" href="#" class="btn-reset">Powrót</a>'.PHP_EOL.
                            '</div>'.PHP_EOL,
        ),
        // Segment 70 (A4, PI+SU+MPU)
        70 => array(
            'header' => 'FORMULARZ REZYGNACJI ZE ZMIANY USŁUG ORAZ ZMIANY ABONAMENTU<br> OD 1 LUTEGO 2022 R.',
            'text' => '<div class="panel">'.PHP_EOL.
                        '<div class="header">'.PHP_EOL.
                         '<p>Dzień dobry,</p>'.PHP_EOL.
                        '</div>'.PHP_EOL.
                        '<div class="info">'.PHP_EOL.
                         '<p>Z nowym pakietem Max Premium zyskasz 10 świeżutkich pozycji na pilocie. A do tego internet światłowodowy, który hula, pędzi, śmiga bez zacinania z jeszcze większą prędkością. Wierzymy, że będziesz z tego zadowolony!</p>'.PHP_EOL.
                         '<p>Jeśli już teraz wiesz, że szybszy internet światłowodowy i zwiększony pakiet TV nie są dla Ciebie i chcesz aby dotychczasowa opłata abonamentowa po 1 lutego 2022 r. pozostała bez zmian, kliknij poniższy przycisk.</p>'.PHP_EOL.
                        '</div>'.PHP_EOL.
                        '<input type="submit" name="confirm" class="submit" value="Potwierdzam" id="confirm">'.PHP_EOL.
                    '</div>'.PHP_EOL,
            'success' => '<div class="panel">'.PHP_EOL.
                            '<div class="info">'.PHP_EOL.
                             '<p>Dziękujemy, zgłoszenie zostało przyjęte do realizacji.</p>'.PHP_EOL.
                             '<p>Twoje usługi oraz abonament po 1 lutego 2022 r. pozostaną bez zmian.</p></div>'.PHP_EOL.
                            '<a id="reset" href="#" class="btn-reset">Powrót</a>'.PHP_EOL.
                        '</div>'.PHP_EOL,
            'in_progress' => '<div class="panel">'.PHP_EOL.
                                '<div class="info">'.PHP_EOL.
                                 '<p>Otrzymaliśmy już Twoje poprzednie zgłoszenie i jest ono w trakcie realizacji.</p>
                                 <p>Twoje usługi oraz abonament po 1 lutego 2022 r. pozostaną bez zmian.</p>'.PHP_EOL.
                                '</div>'.PHP_EOL.
                                '<a id="reset" href="#" class="btn-reset">Powrót</a>'.PHP_EOL.
                            '</div>'.PHP_EOL,
        ),
        // Segment 71 (A5, PI+MPU)
        71 => array(
            'header' => 'FORMULARZ REZYGNACJI ZE ZMIANY USŁUGI ORAZ ZMIANY ABONAMENTU<br> OD 1 LUTEGO 2022 R.',
            'text' => '<div class="panel">'.PHP_EOL.
                        '<div class="header">'.PHP_EOL.
                         '<p>Dzień dobry,</p>'.PHP_EOL.
                        '</div>'.PHP_EOL.
                        '<div class="info">'.PHP_EOL.
                         '<p>Z bogatszym pakietem Max Premium zyskasz 10 nowych kanałów filmowych. Miłośnicy dużej dawki adrenaliny znajdą tutaj kino sensacyjne, thrillery i horrory, a kinomaniacy – kino niezależne i autorskie. Wierzymy, że będziesz z tego zadowolony!</p>'.PHP_EOL.
                         '<p>Jeśli już teraz wiesz, że większy wybór kanałów nie jest dla Ciebie i chcesz aby dotychczasowa opłata abonamentowa po 1 lutego 2022 r. pozostała bez zmian, kliknij poniższy przycisk.</p>'.PHP_EOL.
                        '</div>'.PHP_EOL.
                        '<input type="submit" name="confirm" class="submit" value="Potwierdzam" id="confirm">'.PHP_EOL.
                    '</div>'.PHP_EOL,
            'success' => '<div class="panel">'.PHP_EOL.
                            '<div class="info">'.PHP_EOL.
                             '<p>Dziękujemy, zgłoszenie zostało przyjęte do realizacji.</p>'.PHP_EOL.
                             '<p>Twój pakiet telewizyjny oraz abonament po 1 lutego 2022 r. pozostaną bez zmian.</p></div>'.PHP_EOL.
                            '<a id="reset" href="#" class="btn-reset">Powrót</a>'.PHP_EOL.
                        '</div>'.PHP_EOL,
            'in_progress' => '<div class="panel">'.PHP_EOL.
                                '<div class="info">'.PHP_EOL.
                                 '<p>Otrzymaliśmy już Twoje poprzednie zgłoszenie i jest ono w trakcie realizacji.</p>
                                 <p>Twój pakiet telewizyjny oraz abonament po 1 lutego 2022 r. pozostaną bez zmian.</p>'.PHP_EOL.
                                '</div>'.PHP_EOL.
                                '<a id="reset" href="#" class="btn-reset">Powrót</a>'.PHP_EOL.
                            '</div>'.PHP_EOL,
        )
    )
];