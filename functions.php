<?php
require_once __DIR__ . '/config/config.php';
require_once __DIR__ . '/config/parameters.php';
require_once __DIR__ . '/config/contents.php';
require_once __DIR__ . '/classes/DB.php';
require_once __DIR__ . '/classes/App.php';

header('Access-Control-Allow-Origin: *');

if (__ENVIRONMENT__ === 'dev') {

    error_reporting(E_ALL);
    ini_set('display_errors', '1');
}

$result = array(
    'result' => false
);

if(!isset($_SESSION)){
    if(isset($_GET['sessid']) && $_GET['sessid'] != ""){
        session_id($_GET['sessid']);
    }
    session_start();
}

$app = new App($config, $parameters, $contents);

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $action = filter_input(INPUT_POST, 'action');

    switch ($action) {
        case 'start_session':
                if (!isset($_SESSION)) {
                    session_start();
                }

                $result = array('sesId' => session_id());
            break;
        case 'check_captcha':
                $userCaptcha = filter_input(INPUT_POST, 'recaptchaResponse');
				$isCaptchaValid = $app->checkCaptcha($userCaptcha);
				
				if ($isCaptchaValid == true) {                    
                    $result['result'] = true;
                } else {
					$result = array(
                        'result' => false,
                        'msg' => $app->getErrorMsg('captcha')
                    );
				}
			break;
        case 'exists_account':
                $clientId = filter_input(INPUT_POST, 'id');
                $credential = filter_input(INPUT_POST, 'credential');
                $serviceType = filter_input(INPUT_POST, 'serviceType');
				$isExistsAccount = $app->isExistsAccount($clientId, $serviceType, $credential);
                
                if ($isExistsAccount) {
                    $result['result'] = true;
                } else {
                    $result = array(
                        'result' => false,
                        'msg' => $app->getErrorMsg('unauthorized')
                    );
                }
            break;
        case 'check_action':
                $clientId = filter_input(INPUT_POST, 'id');
                $serviceType = filter_input(INPUT_POST, 'serviceType');
                $app->setId($clientId);
                
                $header = $contents['form_header_params']['default'];
                $content = '';
                $status = false;

				//typ uzytkownika
				$app->setUserType();

                //czy minął czas ważności formularza
                if ($app->isExpired()){
					$expDate = $app->getExpirationDate();
					
					if ($expDate) {
						$formExpirationDate = new \DateTime($app->getExpirationDate());
						$dayBeforeExpiration = $formExpirationDate->modify('-1 day');
						$expiredMsg = $app->getErrorMsg('expired');
						$content = sprintf($expiredMsg, $dayBeforeExpiration->format('d.m.Y'));
					}
					
                    //czy uprawniony do wyswietlenia formularza
                    
                }elseif($app->isAllowed()){ 
                    
                    $header = $app->getFormContent($app->userType, 'header');
                    
                    //czy jest w trakcie realizacji
                    if ($app->isInProcess($serviceType)) {
                        
                        $content = $app->getFormContent($app->userType, 'in_progress');
                    } else {
                        if ($app->isFirstTime()) {
                            $app->insertDb($serviceType);
                        }   
                        
                        $content = $app->getFormContent($app->userType,'text');
                        
                    }
                } else {
                    $content = $app->getErrorMsg('not_allowed');
                }

                $result = array(
                    'result' => true,
                    'msg' => [
                        'header' => $header,
                        'content' => $content
                    ]
                );                
            break;
        case 'send_confirm':
                $clientId = filter_input(INPUT_POST, 'id');
                $serviceType = filter_input(INPUT_POST, 'serviceType');
                $app->setId($clientId);

                if ($app->sendConfirm($serviceType)) {
                    $result = array(
                        'result' => true,
                        'msg' => $app->getFormContent($app->userType, 'success')
                    );
                } else {
                    $result['msg'] = $app->getErrorMsg('db');
                }
            break;
		case 'get_form_header':
        
            $headerParam = filter_input(INPUT_POST, 'headerParam');
            $isEnabled = $app->isFormEnabled($headerParam);
            
            if ($isEnabled === true) {
                
                $header = $contents['form_header_params'][$headerParam] ?? $contents['form_header_params']['default'];
            } else {
                
                $header = $contents['form_header_params']['disabled'];
            }
            

            $result = [
                'result' => true,
                'msg' => [
                    'header' => $header
                ],
                'is_enabled' => $isEnabled
            ];

            break;
        default:
            $result['errorCode'] = 2;
            $result['msg'] = $app->getErrorMsg('no_action');
    }
} else {
    $result['errorCode'] = 1;
    $result['msg'] = $app->getErrorMsg('bad_request');
}

echo json_encode($result);
exit;