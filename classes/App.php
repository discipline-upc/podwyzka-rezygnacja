<?php

class App {
    const captchaKey = 'captcha_result';
    const serviceNameB2C = 'rezygnacja-z-podwyzki';
    const serviceNameB2B = 'rezygnacja-z-podwyzki-b2b';

    private static $captchaResult;

    private $config;
    private $id;
    private $db;
    private $isFirstTime;
    private $serviceType;
	private $expirationDates = [];
    private $expFormDates = null;
    private $recaptchaKey;
    private $recaptchaUrl;
    private $panelUrl;
    private $panelResignationNotificationKey;
    private $contents;
    
    public $userType;
    public $messages;

    public function __construct($config, $parameters, $contents) {
        $this->config = $config;
        $this->db = new DB($config);

		$this->expirationDates = $parameters['expiration_date_per_group'];
        $this->expFormDates = $parameters['first_step_expiration_date'];
        $this->id = 0;
        $this->userType = 0;
        $this->isFirstTime = false;
        $this->recaptchaKey = $parameters['recaptcha']['key'];
        $this->recaptchaUrl = $parameters['recaptcha']['url'];
        $this->panelUrl = $parameters['panel_url'];
        $this->panelResignationNotificationKey = $parameters['panel_resignation_notification_key'];
        $this->contents = $contents;
    }
	
    public function getErrorMsg(string $key) {
        
        if (!isset($this->contents["errors"][$key])) {
            
            return null;
        }
        
        return $this->contents["errors"][$key];
    }
    
    public function getFormContent(string $userType, string $key) {
        
        if (!isset($this->contents["forms"][$userType][$key])) {
            
            return null;
        }
        
        return $this->contents["forms"][$userType][$key];
    }
    
    public function isFormEnabled(string $param = null) {
        
        if ($param !== null) {
                
            $expStr = isset($this->expFormDates[$param]) ? $this->expFormDates[$param] : $this->expFormDates['default'];
            
        } else {
            $expStr = $this->expFormDates['default'];
        }
        
        $expiredTime = new \DateTime($expStr);
        $currentTime = new \DateTime();
        
        return $currentTime < $expiredTime;
    }
    
    public function isExpired() {
        
        $time = $this->getExpirationDate();
		
		if (!$time) {
			return false;
		}
		
        $expiredTime = new \DateTime($time);
        $currentTime = new \DateTime();
    
        return $currentTime < $expiredTime ? false : true;
    }
	
	public function getExpirationDate() {
		return $this->expirationDates[$this->userType] ?? null;
	}

    public function setUserType() {
        $user = $this->db->getUser($this->id);

        if (is_array($user) && isset($user['TYPE'])) {
            $this->userType = $user['TYPE'];
        }
    }

    public function isAllowed() {
        return $this->db->inDatabase($this->id);
    }

    public function isInProcess($serviceType) {
        $this->setServiceType($serviceType);
        $service = $this->getServiceName();
        $resp = $this->db->getUserFromProcess($this->id, $service);

        if (is_array($resp) && count($resp) > 0 && isset($resp[0])) {
            $_resp = $resp[0];

            if (isset($_resp['COMPLETED']) && $_resp['COMPLETED'] == '1') {
                $this->isFirstTime = false;
                return true;
            }
        }

        $this->isFirstTime = true;
        return false;
    }

    public function insertDb($serviceType) {
        $this->setServiceType($serviceType);
        $service = $this->getServiceName();
        $this->db->addUser($this->id, $service);
    }

    public function isFirstTime() {
        return $this->isFirstTime;
    }

    public function sendConfirm($serviceType) {
        $result = false;
        $this->setServiceType($serviceType);
        $service = $this->getServiceName();
        $_user = $this->db->getUser($this->id);

        if (isset($_user['TYPE'])) {
            $this->userType = (int)$_user['TYPE'];
        }

        $updateUser = $this->db->getUserToUpdate($this->id, $service);
        if (is_array($updateUser) && count($updateUser) > 0 && isset($updateUser[0]['ID'])) {
            $result = $this->db->update($this->id, $updateUser[0]['ID'], 1);
            
            if ($result && isset($_user['UUID'])) {
                $this->sendNotification($_user['UUID']);
            }
        }

        return $result;
    }
    
    private function sendNotification($uuid) {
        
        $expDate = $this->getExpirationDate();
        $url = $this->panelUrl."reports/resignations/notification";
        $query = "key={$this->panelResignationNotificationKey}&cid={$uuid}&exp_date={$expDate}";
       
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url . '?' . $query);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_TIMEOUT, '3');
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

        $content = curl_exec($ch);
        
        curl_close($ch);
    }

    public function checkCaptcha($userCaptcha = null) {
    	$isCaptchaValid = false;
		if (!empty($userCaptcha)) {
			$url = $this->recaptchaUrl;
			$key = $this->recaptchaKey;
			$data = array('secret' => $key, 'response' => $userCaptcha);
			$options = array(
				'http' => array(
					'header' => "Content-type: application/x-www-form-urlencoded\r\n",
					'method' => "POST",
					'content' => http_build_query($data)
				)
			);
			$context = stream_context_create($options);
			$jsonResult = file_get_contents($url, false, $context);
			
			if($jsonResult !== false){
				$result = json_decode($jsonResult);
				$isCaptchaValid = $result->success;
			}
		}
		
		return $isCaptchaValid;
	}

    public function checkExistsAccount($id = null, $credential = null) {
        
        $credential = str_replace("-", "", $credential);
        
        if (is_numeric($id) && is_numeric($credential)) {
            return in_array($id, $this->existsAccounts);
        }

        return false;
    }

    public function isExistsAccount($id, $serviceType, $credential) {
        
        if (__ENVIRONMENT__ === 'dev' && isset($_COOKIE['res-test'])) {
            return true;
        }
        
        $this->setServiceType($serviceType);
        if (strpos($this->config['api']['url'], 'app-t') !== false) {
            $url = 'https://app-t.upc.pl/zero-touch/';
        } else {
            $url = 'https://app.upc.pl/zero-touch/';
        }
        $credential = str_replace("-", "", $credential);
        $service = $this->getServiceName();
        if (is_numeric($id) && is_numeric($credential)) {
            $data = array(
                'id' => $id,
                'credential' => $credential,
                'service' => $service,
                'get' => 'auth',
                'format' => 'json'
            );

            $query = http_build_query($data);

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url . '?' . $query);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_TIMEOUT, '3');
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

            $content = curl_exec($ch);

            curl_close($ch);

            $response = json_decode($content, true);
            
            if (isset($response['userInfo']['Status']) && $response['userInfo']['Status'] === "OK") {
                return true;

            }

            return false;
        }
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setServiceType($type){
        $this->serviceType = $type;
    }

    private function getServiceName(){
        return strtolower($this->serviceType) === 'b2b' ? self::serviceNameB2B : self::serviceNameB2C;
    }
}