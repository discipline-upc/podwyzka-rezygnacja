<?php 

class DB {    
    private $config;
    private $conn;

    public function __construct($config) {
        $this->config = $config;
        $this->setConnection();
    }
    
    public function __destruct() {
        oci_close($this->conn);
    }

    private function setConnection() {
        $_host = $this->config['db']['host'];//charon.upc.pl';
        $_sid = $this->config['db']['sid'];//ascon';
        $_port = $this->config['db']['port']; //1521        
        $_tns = "(DESCRIPTION=(ADDRESS_LIST = (ADDRESS = (PROTOCOL = TCP)(HOST = ".$_host.")(PORT = ".$_port.")))(CONNECT_DATA=(SID=".$_sid.")))";

        $this->conn = oci_connect($this->config['db']['user'], $this->config['db']['pass'], $_tns);
    }

    public function inDatabase($clientID = null) {
        if (is_null($clientID) || (int)$clientID < 1) {
            return false;
        }

        $id = (int)$clientID;

        $sql = 'SELECT COUNT(ID) AS ilosc FROM '.$this->config['db']['table_users'].' WHERE client_id = :id';

        $stmt = oci_parse($this->conn, $sql);
        oci_bind_by_name($stmt, ':id', $clientID, -1);
        oci_execute($stmt, OCI_DEFAULT);

        $return = oci_fetch_array($stmt, OCI_ASSOC);

        oci_free_statement($stmt);      

        return (isset($return['ILOSC']) && intval($return['ILOSC']) > 0) ? true : false;
    }

    public function getUser($id) {
        $id = (int)$id;

        $sql = 'SELECT * FROM '.$this->config['db']['table_users'].' WHERE client_id = :id';
        
        $stmt = oci_parse($this->conn, $sql);
        oci_bind_by_name($stmt, ':id', $id, -1);
        oci_execute($stmt, OCI_DEFAULT);

        $return = oci_fetch_assoc($stmt);

        oci_free_statement($stmt);   

        return $return;
    }

    public function getUserFromProcess($id, $service) {
        $id = (int)$id;

        $sql = 'SELECT * FROM '.$this->config['db']['table_process'].' WHERE client_id = :id AND service = :service';

        $stmt = oci_parse($this->conn, $sql);
        oci_bind_by_name($stmt, ':id', $id);
        oci_bind_by_name($stmt, ':service', $service);
        oci_execute($stmt);

        oci_fetch_all($stmt, $result, 0, -1, OCI_FETCHSTATEMENT_BY_ROW);
        oci_free_statement($stmt);   

        return $result;
    }

    public function getUserToUpdate($id, $service) {
        $id = (int)$id;
        $completed = 0;
        
        $sql = 'SELECT ID FROM '.$this->config['db']['table_process'].' WHERE client_id = :id AND service = :service AND completed = :completed';
        $stmt = oci_parse($this->conn, $sql);
        oci_bind_by_name($stmt, ':id', $id);
        oci_bind_by_name($stmt, ':service', $service);
        oci_bind_by_name($stmt, ':completed', $completed);
        oci_execute($stmt);

        oci_fetch_all($stmt, $result, 0, -1, OCI_FETCHSTATEMENT_BY_ROW);
        oci_free_statement($stmt);   

        return $result;
    }

    public function addUser($clientId, $service) {
        $dateNow = date('Y-m-d H:i:s');
        $status = '0';

        $sql_seq = 'SELECT AK_RESIGNATIONS_ID_SEQ.nextval FROM dual';
        $stmt = oci_parse($this->conn, $sql_seq);
        $test = oci_execute($stmt);

        oci_fetch_all($stmt, $result);
        
        $_id = $result['NEXTVAL'][0];

        $sql = 'INSERT INTO '.$this->config['db']['table_process'].' (ID, CLIENT_ID, COMPLETED, AUTHENTICATION_DATE, RESIGNATION_DATE, SERVICE) VALUES(:id, :client_id, :status, current_timestamp, null, :service)';
        
        $stmt = oci_parse($this->conn, $sql);
        oci_bind_by_name($stmt, ':id', $_id);
        oci_bind_by_name($stmt, ':client_id', $clientId);
        oci_bind_by_name($stmt, ':status', $status);
        oci_bind_by_name($stmt, ':service', $service);

        $r = oci_execute($stmt);
      
        oci_free_statement($stmt);
    }

    public function update($clientId, $rowId, $status) {
        $returnStatus = false;
        $sql = 'UPDATE '.$this->config['db']['table_process'].' SET completed = :completed, resignation_date = current_timestamp WHERE id = :id AND client_id = :client_id';

        $stmt = oci_parse($this->conn, $sql);
        oci_bind_by_name($stmt, ':completed', $status);
        oci_bind_by_name($stmt, ':id', $rowId);
        oci_bind_by_name($stmt, ':client_id', $clientId);

        if (oci_execute($stmt)) {
            $returnStatus = true;
        }

        oci_free_statement($stmt);

        return $returnStatus;
    }

    public function test($table = 'table_process') {      
        if ($table === 'user') {
            $table = 'table_users';
        }  

        $sql = 'SELECT * FROM '.$this->config['db'][$table];
        $stid = oci_parse($this->conn, $sql);
        oci_execute($stid);

        oci_fetch_all($stid, $result, 0, -1, OCI_FETCHSTATEMENT_BY_ROW);
        echo '<pre>'; print_r($result);
        exit;
    }
}