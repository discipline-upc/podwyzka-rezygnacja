;(function(){
    var serviceType = 'b2c';
    var credentialName = 'PESEL';
	var headersForGroupParams = [];

    var getUrlParameter = function getUrlParameter(sParam) {
        var sPageURL = decodeURIComponent(window.location.search.substring(1)),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;

        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');

            if (sParameterName[0] === sParam) {
                return sParameterName[1] === undefined ? true : sParameterName[1];
            }
        }
    };
	
    if(getUrlParameter('b2b')){
        serviceType = 'b2b';
        credentialName = 'NIP';
    }
	
    var FormApp = {
		formHeaderContainer: $('#form-header'),
        $container: $('#form-resignation'),
        fields: {},
        errors: [],
        clientData: {
            id: 0,
            credential: 0
        },
        step: 0,
        templateSteps: {
            0: "\<div class='step-content step-one' data-step='0'>\
                    <div class='form-row user'>\
                        <span class='icon user'></span>\
                        <input type='text' name='client_id' placeholder='Numer Klienta'>\
                        <span class='alert'></span>\
                    </div>\
                    <div class='form-row credential'>\
                        <span class='icon user'></span>\
                        <input type='text' name='client_credential' placeholder='Numer "+credentialName+"'>\
                        <span class='alert'></span>\
                    </div>\
                    <div class='form-row'>\
                        <button role='submit' id='next' class='submit'>Dalej</button>\
                    </div>\
                </div>\
                ",
            1: "\<div class='step-content step-two' data-step='1'>\
                    <div id='captcha-container'>\</div>\
                  <p class='captcha-info'>To rozwiązanie (captcha) pomaga nam w blokowaniu złośliwych ataków, a w efekcie powoduje zablokowanie działania mechanizmu.</p>\
                  <div class='form-row prev'><a id='prev' href='#'>Cofnij</a></div>\
                    <div class='form-row'>\
                        <button role='submit' id='next' class='submit'>Dalej</button>\
                    </div>\
                </div>\
                </div>\
                "
        },
        init: function() {
			$(".wrapp-box").hide();
		
            this.getFormHeader();
            this.start();
			
            $(document).delegate('#form-resignation', 'submit', function(e) {
                e.preventDefault();

                FormApp.sendConfirm();
            });

            $(document).delegate('#reset', 'click', function(e){
                e.preventDefault();

                FormApp.start();
            });

            $(document).delegate('#next', 'click', function(e){
				e.preventDefault();

                FormApp.setFields();
                if (FormApp.getStep() === 0) {
					FormApp.validationData();
                } else {
					FormApp.checkCaptcha();
				}
            });

            $(document).delegate('#prev', 'click', function(e){
                e.preventDefault();
                FormApp.start();
            });

            $(document).delegate('input', 'keydown', function(e) {
                if (e.which === 32){
                    return false;
                }
            });
            $(document).delegate('input', 'change', function(e) {
                this.value = this.value.replace(/\s/g, "");
            });
        },
        endProcess: function() {
			this.fields = {};
            this.errors = [];
            this.clientData = {
                id: 0,
                credential: 0
            };
            this.step = 0;
        },
        setFormHeader: function(value) {
		    this.formHeaderContainer.html(value);
			$(".wrapp-box").show();
		},
        getStep: function() {
            return this.step;
        },
        setFields: function() {
            this.fields = this.$container.serializeArray();
        },
        validationData: function() {
            var count = this.fields.length,
                tmpErrors = [],
                i = 0;

            this.errors = [];

            for(i; i<count; i++) {
                var pattern = '',
                    msg = '',
                    value = FormApp.fields[i].value,
                    fieldName = FormApp.fields[i].name;

                switch (fieldName) {
                    case 'client_id':
                        pattern = /^[\d+]{3,7}$/;
                        msg = 'Proszę wprowadzić poprawny numer Klienta';
                        break;
                    case 'client_credential':
                        pattern = /^\d{10}$|^\d{3}\-?\d{3}\-?\d{2}\-?\d{2}$/;
                        if(credentialName === 'PESEL'){
                            pattern = /^\d{11}$/;
                        }
                        msg = 'Proszę wprowadzić poprawny numer ' + credentialName;
                        break;
                }

                if (!value.match(pattern)) {
                    //tmpErrors.push({field: fieldName, msg: msg});
                    this.errors.push({field: fieldName, msg: msg});
                } else {
                    FormApp.setData(fieldName, value);
                }
            }

            if (this.errors.length > 0) {
                this.displayErrors();
            } else {
                this.step++;
                this.nextStep(this.templateSteps[this.step]);
                this.renderCaptcha();
            }
        },
        renderCaptcha: function() {
            var onloadCallback = function() {
                captchaContainer = grecaptcha.render('captcha-container', {
					// app
					// 'sitekey' : '6LdirJIUAAAAALfJhOkaFcRVHf_L4JP6SA7hAGkc',
                    // app-t
					'sitekey' : '6LfoM5IUAAAAANEjlhFjBBur0juXvPF9EzNSnPv0',
                    'callback' : function(response) {
                        // console.log(response);
                    }
                });
            };
            onloadCallback();
        },
		getFormHeader: function() {
            var headerParameter = getUrlParameter('f') ? getUrlParameter('f') : 'default';
            var headers = $.ajax({
                url: 'functions.php',
                method: 'POST',
                dataType: 'json',
                data: {action: 'get_form_header', headerParam: headerParameter}
            });

			$.when(headers).then(function(resHeaders){
                
                var formHeader = '';
                
                if (resHeaders.result && typeof resHeaders.msg.header !== 'undefined') {
                    
                    formHeader = resHeaders.msg.header;
                    
                }
                
                if (resHeaders['is_enabled'] === true) {
                
                    FormApp.setFormHeader(formHeader);
                    
                } else {
                    
                    $(".content").hide();
                    FormApp.setFormHeader(formHeader);
                    $("#form-header").after('<div class="panel"><div class="error-content">Ta strona nie jest już dostępna.</div></div>');
                }
                
            });
        },
        displayErrors: function() {
            var errors = this.errors,
                count = errors.length,
                i = 0;

            $('.form-row')
                .removeClass('error')
                .children('.alert')
                .empty();

            for(i;i<count;i++) {
                $('[name="' + errors[i].field + '"]')
                    .parent('.form-row')
                    .addClass('error')
                    .children('.alert')
                    .text(errors[i].msg);
            }
        },
        nextStep: function(contentHtml) {
            this.$container
                .empty()
                .append(contentHtml);
        },
		checkCaptcha: function() {
            var _this = this;
			userCaptcha = _this.fields[0].value;
			
			var checkingCaptcha =  $.ajax({
                url: 'functions.php',
                method: 'POST',
                dataType: 'json',
                data: {action: 'check_captcha', recaptchaResponse: userCaptcha}
            });

			$.when(checkingCaptcha).then(function(respCaptcha){
				if (respCaptcha.result) {
                    _this.validationDataAjax();
				} else {
                    if ($('.captcha-error').length == 0) {
                       $('#captcha-container').after('<div class="captcha-error">' + respCaptcha.msg + '</div>'); 
                    }
				}
			});
		},
        validationDataAjax: function() {
            this.showLoader();
            
			var id = this.clientData.id,
                credential = this.clientData.credential;
            this.errors = [];

            var isExists = $.ajax({
                url: 'functions.php',
                method: 'POST',
                dataType: 'json',
                data: {action: 'exists_account', id: id, credential: credential, serviceType: serviceType}
            });
            var userAction = $.ajax({
                url: 'functions.php',
                method: 'POST',
                dataType: 'json',
                data: {action: 'check_action', id: id}
            });
			
            $.when(isExists, userAction).then(function(respAccount, respAction) {
                var oAccount = respAccount[0],
                    oAction = respAction[0];

                if (oAccount.result && oAction.result) {
                    FormApp.setFormHeader(oAction.msg.header);
                    FormApp.setNewContent(oAction.msg.content);
                } else if(oAccount.result !== true) {
                    FormApp.setNewContent(oAccount.msg);
                }

                FormApp.hideLoader();
            });
        },
        setNewContent: function(msg) {
            this.$container
                .empty()
                .html(msg).show();
        },
        setData: function(fName, fValue) {
            if (fName === 'client_id') {
                this.clientData.id = fValue;
            } else if (fName === 'client_credential') {
                this.clientData.credential = fValue;
            }
        },
        start: function(msg) {
            this.fields = {};
            this.errors = [];
            this.clientData = {
                id: 0,
                credential: 0
            };
			
            this.step = 0;

			var _msg = this.templateSteps[0];

            if (msg) {
                _msg = msg;
            }

            this.$container
                .empty()
                .html(_msg).show();
        },
        showLoader: function() {
            this.$container.parent('.content').addClass('show-loader');
        },
        hideLoader: function() {
            this.$container.parent('.content').removeClass('show-loader');
        },
        hideErrors: function() {
            var rows = this.$container.find('.form-row');
        },
        sendConfirm: function() {
            var id = this.clientData.id;

            $.ajax({
                url: 'functions.php',
                method: 'POST',
                dataType: 'json',
                data: {action: 'send_confirm', id: id},
                success: function(data) {
                    if (data.hasOwnProperty('msg')) {
                        FormApp.setNewContent(data.msg);
                    }
                }
            });
        }
    };

    FormApp.init();
})();